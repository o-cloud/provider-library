package utils

import (
	"math/rand"
	"time"
)

func GenerateRandomStrongPassword() string {
	charSet := "RW!Vx&6fMHyPYSB1da*4LTm3ki@5c2ptgDzZ9Gq8w7Ke$XNE#s_jvrJuQnFCAUbh"
	pass := randomStringGenerator(charSet, 12)
	return pass
}

func randomStringGenerator(charSet string, codeLength int32) string {
	code := ""
	rand.Seed(time.Now().UnixNano())
	charSetLength := int32(len(charSet))
	for i := int32(0); i < codeLength; i++ {
		index := randomNumber(0, charSetLength)
		code += string(charSet[index])
	}

	return code
}

func randomNumber(min, max int32) int32 {
	return min + int32(rand.Intn(int(max-min)))
}
