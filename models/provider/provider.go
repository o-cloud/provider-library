package provider

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	catalog "gitlab.com/o-cloud/catalog/api/intern"
	providerType "gitlab.com/o-cloud/catalog/api/public"
	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/config"
	"gitlab.com/o-cloud/provider-library/models/scopes"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetProviderRepository() ProviderRepository {
	return &providerRepository{
		MongoClient:     common.GetMongoClient().Collection(common.ProvidersCollection),
		CatalogClient:   catalog.NewClient(config.Config.CatalogUrl),
		ProviderName:    config.Config.Name,
		ProviderVersion: config.Config.Version,
		ProviderType:    providerType.ProviderType(config.Config.Type),
		ProviderCwl:     loadCwlFromFile(config.Config.CwlFileName),
	}
}

type ProviderRepository interface {
	GetProvider() (*Provider, error)
	GetProviderCwl() string
	CreateProvider() error
	UpdateProvider(provider *Provider) error
	DeleteProvider() error
	RegisterProvider(provider *Provider)
	UnregisterProvider(provider *Provider)
}

type providerRepository struct {
	MongoClient     *mongo.Collection
	CatalogClient   catalog.InternalCatalogAPI
	ProviderName    string
	ProviderVersion string
	ProviderType    providerType.ProviderType
	ProviderCwl     string
}

type catalogSyncStatus int

const (
	Unregistered catalogSyncStatus = iota
	Registered
)

type Provider struct {
	Id primitive.ObjectID `bson:"_id,omitempty"`

	Name        string                    `bson:"name,omitempty"`
	Description string                    `bson:"description,omitempty"`
	Version     string                    `bson:"version,omitempty"`
	Type        providerType.ProviderType `bson:"type,omitempty"`
	Images      []string                  `bson:"images"`
	Scopes      scopes.Scopes             `bson:"scopes,omitempty"`

	CatalogRegistered catalogSyncStatus `bson:"catalog_sync_status"`
	CreatedAt         time.Time         `bson:"created_at,omitempty"`
	UpdatedAt         time.Time         `bson:"updated_at,omitempty"`
	RegisteredAt      time.Time         `bson:"registered_at,omitempty"`
	UnregisteredAt    time.Time         `bson:"unregistered_at,omitempty"`
}

func (p *providerRepository) GetProvider() (*Provider, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result := p.MongoClient.FindOne(ctx, bson.M{"name": p.ProviderName, "version": p.ProviderVersion})
	err := result.Err()

	if err == mongo.ErrNoDocuments {
		err = fmt.Errorf("provider %s with version %s not found", p.ProviderName, p.ProviderVersion)
		return nil, err
	}

	if err != nil {
		panic(err)
	}

	// create a value into which the single document can be decoded
	var provider Provider
	err = result.Decode(&provider)
	if err != nil {
		panic(err)
	}

	return &provider, nil
}

func (p *providerRepository) GetProviderCwl() string {
	return p.ProviderCwl
}

func (p *providerRepository) CreateProvider() error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	provider := Provider{
		Name:              p.ProviderName,
		Version:           p.ProviderVersion,
		Type:              p.ProviderType,
		CatalogRegistered: Unregistered,
		CreatedAt:         time.Now(),
		UpdatedAt:         time.Now(),
	}

	_, err := p.MongoClient.InsertOne(ctx, provider)
	if mongo.IsDuplicateKeyError(err) {
		err = fmt.Errorf("provider %s with version %s already exists", provider.Name, provider.Version)
		return err
	}

	if err != nil {
		panic(err)
	}

	return nil
}

func (p *providerRepository) UpdateProvider(provider *Provider) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	provider.UpdatedAt = time.Now()

	_, err := p.MongoClient.UpdateOne(ctx, bson.M{"name": p.ProviderName, "version": p.ProviderVersion}, bson.D{{Key: "$set", Value: provider}})

	if mongo.IsDuplicateKeyError(err) {
		err = fmt.Errorf("provider %s with version %s already exists", provider.Name, provider.Version)
		return err
	}

	if err != nil {
		panic(err)
	}

	return nil
}

func (p *providerRepository) DeleteProvider() error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, err := p.MongoClient.DeleteOne(ctx, bson.M{"name": p.ProviderName, "version": p.ProviderVersion})

	if result.DeletedCount == 0 {
		err = fmt.Errorf("provider %s with version %s not found", p.ProviderName, p.ProviderVersion)
		return err
	}

	if err != nil {
		panic(err)
	}

	return nil
}

func (p *providerRepository) RegisterProvider(provider *Provider) {

	providerInfo := catalog.ProviderInternalInfo{
		ProviderId:        provider.Id.Hex(),
		Type:              provider.Type,
		Version:           provider.Version,
		Name:              provider.Name,
		Description:       provider.Description,
		Metadata:          map[string]interface{}{"images": provider.Images},
		EndpointLocation:  "{{CLUSTER_HOST}}" + config.Config.Server.ApiPrefix,
		WorkflowStructure: p.ProviderCwl,
	}

	if err := p.CatalogClient.RegisterDataProvider(context.TODO(), providerInfo); err != nil {
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	now := time.Now()
	result, err := p.MongoClient.UpdateByID(ctx, provider.Id, bson.M{"$set": bson.M{"catalog_sync_status": Registered, "registered_at": now}})

	if result.MatchedCount == 0 {
		err = fmt.Errorf("unable to declare provider %s version %s to be registered", p.ProviderName, p.ProviderVersion)
		panic(err)
	}

	if err != nil {
		panic(err)
	}

	provider.CatalogRegistered = Registered
	provider.RegisteredAt = now
}

func (p *providerRepository) UnregisterProvider(provider *Provider) {
	if err := p.CatalogClient.RemoveProvider(context.TODO(), provider.Type, provider.Id.Hex()); err != nil {
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	now := time.Now()
	result, err := p.MongoClient.UpdateByID(ctx, provider.Id, bson.M{"$set": bson.M{"catalog_sync_status": Unregistered, "unregistered_at": now}})

	if result.MatchedCount == 0 {
		err = fmt.Errorf("unable to declare provider %s version %s to be unregistered", p.ProviderName, p.ProviderVersion)
		panic(err)
	}

	if err != nil {
		panic(err)
	}

	provider.CatalogRegistered = Unregistered
	provider.UnregisteredAt = now
}

func loadCwlFromFile(fileName string) string {
	content, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatal(err)
	}
	return string(content)
}
