package permissions

var availableAccessPermissions AccessPermissions

func GetAccessPermissionsRepository() AccessPermissionsRepository {
	return &accessPermissionsRepository{}
}

type AccessPermissionsRepository interface {
	GetAccessPermissions() AccessPermissions
	RegisterAccessPermission(accessName AccessPermission)
	CheckPermissionExist(accessName string) bool
}

type accessPermissionsRepository struct{}

type AccessPermissions []AccessPermission

type AccessPermission string

const (
	AccessReadOnly  AccessPermission = "ReadOnly"
	AccessWriteOnly AccessPermission = "WriteOnly"
	AccessReadWrite AccessPermission = "ReadWrite"
)

func (a *accessPermissionsRepository) RegisterAccessPermission(accessName AccessPermission) {
	if availableAccessPermissions == nil {
		availableAccessPermissions = make(AccessPermissions, 0)
	}

	availableAccessPermissions = append(availableAccessPermissions, accessName)
}

func (a *accessPermissionsRepository) CheckPermissionExist(accessName string) bool {
	for _, permission := range availableAccessPermissions {
		if string(permission) == accessName {
			return true
		}
	}
	return false
}

func (a *accessPermissionsRepository) GetAccessPermissions() AccessPermissions {
	return availableAccessPermissions
}
