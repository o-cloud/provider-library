package resources

import "fmt"

var resourcesRepository ResourcesRepository

func GetResourcesRepository() ResourcesRepository {
	if resourcesRepository != nil {
		return resourcesRepository
	}
	return &DefaultResourcesRepository{}
}

func CreateResourcesRepository(newResourcesRepository ResourcesRepository) {
	if resourcesRepository == nil {
		resourcesRepository = newResourcesRepository
	}
}

type ResourcesRepository interface {
	ListResources() *[]Resource
	GetResource(resourceName string) (*Resource, error)
	IsResourceExist(resourceName string) bool
}

type Resources []Resource

type Resource struct {
	Name     string `bson:"name,omitempty"`
	Category string `bson:"category,omitempty"`
}

type DefaultResourcesRepository struct {
	ResourcesRepository
}

func (r *DefaultResourcesRepository) GetResource(resourceName string) (*Resource, error) {
	for _, resource := range *r.ListResources() {
		if resourceName == resource.Name {
			return &resource, nil
		}
	}

	return nil, fmt.Errorf("resource %s not found", resourceName)
}

func (r *DefaultResourcesRepository) IsResourceExist(resourceName string) bool {
	for _, resource := range *r.ListResources() {
		if resource.Name == resourceName {
			return true
		}
	}
	return false
}
