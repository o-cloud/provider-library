package access_object

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/config"
	"gitlab.com/o-cloud/provider-library/models/access_tokens"
	"gitlab.com/o-cloud/provider-library/models/scopes"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var accessObjectController AccessObjectController

func GetAccessObjectRepository() AccessObjectRepository {
	return &DefaultAccessObjectRepository{
		MongoClient:           common.GetMongoClient().Collection(common.AccessObjectCollection),
		AccessTokenRepository: access_tokens.GetAccessTokensRepository(),
		ScopesRepository:      scopes.GetScopesRepository(),
		ProviderName:          config.Config.Name,
		ProviderVersion:       config.Config.Version,
	}
}

func SetAccessObjectController(newAccessObjectController AccessObjectController) {
	if accessObjectController == nil {
		accessObjectController = newAccessObjectController
	}
}

type AccessObjectController interface {
	GenerateAccess(scope *scopes.Scope, accessToken access_tokens.AccessToken) ConnectionInformation
	PurgeAccess(connInfo ConnectionInformation)
}

type AccessObjectRepository interface {
	ListAccessObjects(accessToken *access_tokens.AccessToken) *[]AccessObject
	CreateAccessObject(accessToken *access_tokens.AccessToken) (*AccessObject, error)
	DeleteAccessObject(accessToken *access_tokens.AccessToken)
}

type DefaultAccessObjectRepository struct {
	AccessObjectRepository

	MongoClient           *mongo.Collection
	AccessTokenRepository access_tokens.AccessTokensRepository
	ScopesRepository      scopes.ScopesRepository
	ProviderName          string
	ProviderVersion       string
}

type ConnectionInformation interface{}

type AccessObject struct {
	ConnectionInformation

	Id primitive.ObjectID `bson:"_id,omitempty"`

	AccessToken primitive.ObjectID `bson:"access_token_id,omitempty"`
	EndAt       time.Time          `bson:"end_at,omitempty"`

	CreatedAt time.Time `bson:"created_at,omitempty"`
	UpdatedAt time.Time `bson:"updated_at,omitempty"`
}

func (a *DefaultAccessObjectRepository) ListAccessObjects(accessToken *access_tokens.AccessToken) *[]AccessObject {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	cursor, err := a.MongoClient.Find(ctx, bson.M{"access_token_id": accessToken.Id})
	if err != nil {
		panic(err)
	}

	var docs []bson.M
	if err = cursor.All(context.TODO(), &docs); err != nil {
		panic(err)
	}

	var accessObjects []AccessObject
	for _, doc := range docs {
		connectioninformation, _ := json.Marshal(doc["connectioninformation"])
		accessObjects = append(accessObjects, AccessObject{
			ConnectionInformation: connectioninformation,
			Id:                    doc["_id"].(primitive.ObjectID),
			AccessToken:           doc["access_token_id"].(primitive.ObjectID),
			CreatedAt:             doc["created_at"].(primitive.DateTime).Time(),
			UpdatedAt:             doc["updated_at"].(primitive.DateTime).Time(),
		})
	}

	if len(accessObjects) == 0 {
		return &accessObjects
	}

	return &accessObjects
}

func (a *DefaultAccessObjectRepository) CreateAccessObject(accessToken *access_tokens.AccessToken) (*AccessObject, error) {
	scope, err := a.ScopesRepository.GetScopeById(accessToken.Scope.Hex())
	if err != nil {
		err = fmt.Errorf("scope with id %s for provider %s version %s not exist anymore", accessToken.Scope.Hex(), a.ProviderName, a.ProviderVersion)
		return nil, err
	}

	connInfo := accessObjectController.GenerateAccess(scope, *accessToken)

	accessObject := AccessObject{ConnectionInformation: connInfo}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	accessObject.AccessToken = accessToken.Id
	accessObject.CreatedAt = time.Now()
	accessObject.UpdatedAt = time.Now()

	_, err = a.MongoClient.InsertOne(ctx, accessObject)
	if err != nil {
		panic(err)
	}

	return &AccessObject{
		ConnectionInformation: connInfo, EndAt: accessToken.EndAt,
	}, nil

}

func (a *DefaultAccessObjectRepository) DeleteAccessObject(accessToken *access_tokens.AccessToken) {
	accessObjects := a.ListAccessObjects(accessToken)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	for _, accessObject := range *accessObjects {
		accessObjectController.PurgeAccess(accessObject.ConnectionInformation)
		if _, err := a.MongoClient.DeleteOne(ctx, bson.M{"_id": accessObject.Id}); err != nil {
			panic(err)
		}
	}

	a.AccessTokenRepository.DeleteAccessToken(accessToken)
}
