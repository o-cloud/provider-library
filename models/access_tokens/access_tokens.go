package access_tokens

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/config"
	"gitlab.com/o-cloud/provider-library/services/jwt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetAccessTokensRepository() AccessTokensRepository {
	return &accessTokensRepository{
		MongoClient:     common.GetMongoClient().Collection(common.AccessTokensCollection),
		ProviderName:    config.Config.Name,
		ProviderVersion: config.Config.Version,
	}
}

type AccessTokensRepository interface {
	CreateAccessToken(accessToken *AccessToken) (string, error)
	GetAccessToken(token string) (*AccessToken, error)
	GetAccessTokenFromScope(scopeIdHex string) *[]AccessToken
	ListExpiredAccessToken() *[]AccessToken
	DeleteAccessToken(accessToken *AccessToken)
}

type AccessToken struct {
	Id primitive.ObjectID `bson:"_id,omitempty"`

	Signature       string             `bson:"signature,omitempty"`
	ProviderName    string             `bson:"provider_name,omitempty"`
	ProviderVersion string             `bson:"provider_version,omitempty"`
	Scope           primitive.ObjectID `bson:"scope_id,omitempty"`
	Identities      []string           `bson:"identities,omitempty"`
	EndAt           time.Time          `bson:"end_at,omitempty"`

	Params map[string]interface{} `bson:"params"`

	CreatedAt time.Time `bson:"created_at,omitempty"`
	UpdatedAt time.Time `bson:"updated_at,omitempty"`
}

type accessTokensRepository struct {
	MongoClient     *mongo.Collection
	ProviderName    string
	ProviderVersion string
}

func (a *accessTokensRepository) CreateAccessToken(accessToken *AccessToken) (string, error) {

	// Create the Signed token
	issuer := a.ProviderName + "-" + a.ProviderVersion
	ss := jwt.GenerateSignedToken(accessToken.EndAt.Unix(), issuer)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	accessToken.Signature = ss
	accessToken.ProviderName = a.ProviderName
	accessToken.ProviderVersion = a.ProviderVersion
	accessToken.CreatedAt = time.Now()
	accessToken.UpdatedAt = time.Now()

	_, err := a.MongoClient.InsertOne(ctx, accessToken)
	if mongo.IsDuplicateKeyError(err) {
		err = fmt.Errorf(
			"access token with signature %s for provider %s version %s already exists",
			accessToken.Signature,
			a.ProviderName,
			a.ProviderVersion,
		)
		return "", err
	}

	if err != nil {
		panic(err)
	}

	return ss, nil
}

func (a *accessTokensRepository) GetAccessToken(token string) (*AccessToken, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result := a.MongoClient.FindOne(ctx, bson.M{"signature": token, "provider_name": a.ProviderName, "provider_version": a.ProviderVersion})
	err := result.Err()

	if err == mongo.ErrNoDocuments {
		err = fmt.Errorf("access token with signature %s for provider %s version %s not exist anymore", token, a.ProviderName, a.ProviderVersion)
		return nil, err
	}

	if err != nil {
		panic(err)
	}

	// create a value into which the single document can be decoded
	var accessToken AccessToken
	if err = result.Decode(&accessToken); err != nil {
		panic(err)
	}

	return &accessToken, nil
}

func (a *accessTokensRepository) GetAccessTokenFromScope(scopeIdHex string) *[]AccessToken {
	scopeId := common.HexToPrimitiveIdRef(scopeIdHex)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	cursor, err := a.MongoClient.Find(ctx, bson.M{"scope_id": scopeId, "provider_name": a.ProviderName, "provider_version": a.ProviderVersion})

	if err != nil {
		panic(err)
	}

	var accessTokens []AccessToken
	if err = cursor.All(context.TODO(), &accessTokens); err != nil {
		panic(err)
	}

	return &accessTokens
}

func (a *accessTokensRepository) ListExpiredAccessToken() *[]AccessToken {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	cursor, err := a.MongoClient.Find(ctx, bson.M{
		"end_at":        bson.M{"$lt": time.Now()},
		"provider_name": a.ProviderName, "provider_version": a.ProviderVersion,
	})

	if err != nil {
		panic(err)
	}

	var accessTokens []AccessToken
	if err = cursor.All(context.TODO(), &accessTokens); err != nil {
		panic(err)
	}

	if len(accessTokens) == 0 {
		return &accessTokens
	}

	return &accessTokens
}

func (a *accessTokensRepository) DeleteAccessToken(accessToken *AccessToken) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err := a.MongoClient.DeleteOne(ctx, bson.M{"_id": accessToken.Id})
	if err != nil {
		panic(err)
	}
}
