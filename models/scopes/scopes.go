package scopes

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/config"
	"gitlab.com/o-cloud/provider-library/models/permissions"
	"gitlab.com/o-cloud/provider-library/models/resources"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetScopesRepository() ScopesRepository {
	return &scopesRepository{
		MongoClient:     common.GetMongoClient().Collection(common.ProvidersCollection),
		ProviderName:    config.Config.Name,
		ProviderVersion: config.Config.Version,
	}
}

type ScopesRepository interface {
	GetScopeById(id string) (*Scope, error)
	GetScopeByName(name string) (*Scope, error)
	ListScopes() *[]Scope
	CreateScope(scope *Scope) error
	UpdateScope(id string, scope *Scope) error
	DeleteScope(id string) error
}

type scopesRepository struct {
	MongoClient     *mongo.Collection
	ProviderName    string
	ProviderVersion string
}

type Scopes []Scope

type Scope struct {
	Id primitive.ObjectID `bson:"_id,omitempty"`

	Name        string                        `bson:"name,omitempty"`
	Description string                        `bson:"description,omitempty"`
	Resources   resources.Resources           `bson:"resources,omitempty"`
	Permissions permissions.AccessPermissions `bson:"permissions,omitempty"`

	CreatedAt time.Time `bson:"created_at,omitempty"`
	UpdatedAt time.Time `bson:"updated_at,omitempty"`
}

func (s *scopesRepository) GetScopeById(idHex string) (*Scope, error) {
	id := common.HexToPrimitiveIdRef(idHex)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	matchProviderStage := bson.D{{Key: "$match", Value: bson.M{"name": s.ProviderName, "version": s.ProviderVersion}}}
	unwindStage := bson.D{{Key: "$unwind", Value: "$scopes"}}
	replaceRootStage := bson.D{{Key: "$replaceRoot", Value: bson.M{"newRoot": "$scopes"}}}
	matchScopeStage := bson.D{{Key: "$match", Value: bson.M{"_id": id}}}

	cursor, err := s.MongoClient.Aggregate(ctx, mongo.Pipeline{matchProviderStage, unwindStage, replaceRootStage, matchScopeStage})

	if err != nil {
		panic(err)
	}

	var scopes []Scope
	if err = cursor.All(context.TODO(), &scopes); err != nil {
		panic(err)
	}

	if len(scopes) == 0 {
		err = fmt.Errorf("scope with id %s not found in provider %s version %s", id, s.ProviderName, s.ProviderVersion)
		return nil, err
	}

	return &scopes[0], nil
}

func (s *scopesRepository) GetScopeByName(name string) (*Scope, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	matchProviderStage := bson.D{{Key: "$match", Value: bson.M{"name": s.ProviderName, "version": s.ProviderVersion}}}
	unwindStage := bson.D{{Key: "$unwind", Value: "$scopes"}}
	replaceRootStage := bson.D{{Key: "$replaceRoot", Value: bson.M{"newRoot": "$scopes"}}}
	matchScopeStage := bson.D{{Key: "$match", Value: bson.M{"name": name}}}

	cursor, err := s.MongoClient.Aggregate(ctx, mongo.Pipeline{matchProviderStage, unwindStage, replaceRootStage, matchScopeStage})

	if err != nil {
		panic(err)
	}

	var scopes []Scope
	if err = cursor.All(context.TODO(), &scopes); err != nil {
		panic(err)
	}

	if len(scopes) == 0 {
		err = fmt.Errorf("scope with name %s not found in provider %s version %s", name, s.ProviderName, s.ProviderVersion)
		return nil, err
	}

	return &scopes[0], nil
}

func (s *scopesRepository) ListScopes() *[]Scope {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.M{"name": s.ProviderName, "version": s.ProviderVersion}}}
	unwindStage := bson.D{{Key: "$unwind", Value: "$scopes"}}
	replaceRootStage := bson.D{{Key: "$replaceRoot", Value: bson.M{"newRoot": "$scopes"}}}

	cursor, err := s.MongoClient.Aggregate(ctx, mongo.Pipeline{matchStage, unwindStage, replaceRootStage})

	if err != nil {
		panic(err)
	}

	var scopes []Scope
	if err = cursor.All(context.TODO(), &scopes); err != nil {
		panic(err)
	}

	if len(scopes) == 0 {
		return &scopes
	}

	return &scopes
}

func (s *scopesRepository) CreateScope(scope *Scope) error {
	if _, err := s.GetScopeByName(scope.Name); err == nil {
		err := fmt.Errorf("scope with name %s already exist in provider %s version %s", scope.Name, s.ProviderName, s.ProviderVersion)
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	scope.Id = primitive.NewObjectID()
	scope.CreatedAt = time.Now()
	scope.UpdatedAt = time.Now()

	_, err := s.MongoClient.UpdateOne(
		ctx,
		bson.M{"name": s.ProviderName, "version": s.ProviderVersion},
		bson.M{"$addToSet": bson.M{"scopes": scope}, "$set": bson.M{"update_at": scope.UpdatedAt}},
	)

	if err != nil {
		panic(err)
	}

	return nil
}

func (s *scopesRepository) UpdateScope(idHex string, scope *Scope) error {
	id := common.HexToPrimitiveIdRef(idHex)

	if scopeExisting, err := s.GetScopeByName(scope.Name); err == nil && id != scopeExisting.Id {
		err := fmt.Errorf("scope with name %s already exist in provider %s version %s", scope.Name, s.ProviderName, s.ProviderVersion)
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	scope.Id = id
	scope.UpdatedAt = time.Now()

	_, err := s.MongoClient.UpdateOne(
		ctx,
		bson.M{"name": s.ProviderName, "version": s.ProviderVersion, "scopes._id": id},
		bson.M{"$set": bson.M{"scopes.$": scope, "update_at": scope.UpdatedAt}},
	)

	if err != nil {
		panic(err)
	}

	return nil
}

func (s *scopesRepository) DeleteScope(idHex string) error {
	id := common.HexToPrimitiveIdRef(idHex)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, err := s.MongoClient.UpdateOne(
		ctx,
		bson.M{"name": s.ProviderName, "version": s.ProviderVersion, "scopes._id": id},
		bson.M{
			"$pull": bson.M{"scopes": bson.M{"_id": id}},
			"$set":  bson.M{"update_at": time.Now()},
		},
	)

	if result.MatchedCount == 0 {
		err = fmt.Errorf("scope with id %s not found in provider %s version %s", id, s.ProviderName, s.ProviderVersion)
		return err
	}

	if err != nil {
		panic(err)
	}

	return nil
}
