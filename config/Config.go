package config

var (
	Config ProviderConfig
)

func Load(config ProviderConfig) {
	Config = config
}

type ProviderConfig struct {
	Name                       string
	Version                    string
	Type                       string
	CwlFileName                string
	TokenValidityHoursDuration int64
	Server                     ServerServiceConfig
	MongoDatabase              MongoDatabaseConfig
	CatalogUrl                 string
	JwtSecretKey               string
}

type ServerServiceConfig struct {
	ListenAddress string
	ApiPrefix     string
}

type MongoDatabaseConfig struct {
	Name      string
	ConString string
}

type CwlInput struct {
	Name      string
	Type      string
	InputType string
}
