package provider

import (
	"context"
	"log"

	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/config"
	provider "gitlab.com/o-cloud/provider-library/models/provider"
	accessRoute "gitlab.com/o-cloud/provider-library/routers/access"
	permissionsRoute "gitlab.com/o-cloud/provider-library/routers/permissions"
	providerRoute "gitlab.com/o-cloud/provider-library/routers/provider"
	resourcesRoute "gitlab.com/o-cloud/provider-library/routers/resources"
	scopesRoute "gitlab.com/o-cloud/provider-library/routers/scopes"
	"gitlab.com/o-cloud/provider-library/services/access_management"
	"golang.org/x/sync/errgroup"

	"github.com/gin-gonic/gin"
)

func InitProvider(providerConfig config.ProviderConfig) {
	config.Load(providerConfig)
	common.LoadMongoClient()
	provider.GetProviderRepository().CreateProvider()
}

func RunProvider() {
	g, ctx := errgroup.WithContext(context.Background())

	g.Go(access_management.CheckAccessValidity)
	g.Go(startApiServer)

	<-ctx.Done()
	log.Println("Unable to recover from error. Program shutdown")
}

func startApiServer() error {
	r := setupRouter(config.Config.Server.ApiPrefix)

	err := r.Run(config.Config.Server.ListenAddress)
	if err != nil {
		log.Fatal(err)
	}
	return err
}

func setupRouter(apiPrefix string) *gin.Engine {
	r := gin.Default()

	prefixgroup := r.Group(apiPrefix)

	permissionsRoute.NewAccessPermissionsHandler().SetupRoutes(prefixgroup.Group("/permissions"))
	resourcesRoute.NewResourcesHandler().SetupRoutes(prefixgroup.Group("/resources"))
	providerRoute.NewProviderHandler().SetupRoutes(prefixgroup.Group("/provider"))
	scopesRoute.NewScopesHandler().SetupRoutes(prefixgroup.Group("/provider/scopes"))
	accessRoute.NewProviderHandler().SetupRoutes(prefixgroup.Group("/provider/access"))

	return r
}
