package access_management

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/o-cloud/provider-library/models/access_object"
	"gitlab.com/o-cloud/provider-library/models/access_tokens"
)

func CheckAccessValidity() (err error) {
	defer func() {
		if r := recover(); r != nil {
			var ok bool
			err, ok = r.(error)
			if !ok {
				err = fmt.Errorf("pkg: %v", r)
			}
			log.Fatalf("Fatal Error: %s", err)
		}
	}()

	log.Println("Check access validity running...")

	accessTokensRepository := access_tokens.GetAccessTokensRepository()
	accessObjectsRepository := access_object.GetAccessObjectRepository()
	for {
		// Wait five seconds
		time.Sleep(5 * time.Second)

		// Get all expired tokens to purge
		expiredTokens := accessTokensRepository.ListExpiredAccessToken()

		for _, expiredToken := range *expiredTokens {
			log.Println("Expired tokens: ", expiredToken.Id, expiredToken.Identities, expiredToken.EndAt)
			accessObjectsRepository.DeleteAccessObject(&expiredToken)
		}
	}
}
