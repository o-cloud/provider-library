package jwt

import (
	"github.com/golang-jwt/jwt"
	"gitlab.com/o-cloud/provider-library/config"
)

func getSecretKey() []byte {
	return []byte(config.Config.JwtSecretKey)
}

func GenerateSignedToken(expireAt int64, issuer string) string {
	claims := jwt.StandardClaims{ExpiresAt: expireAt, Issuer: issuer}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString(getSecretKey())
	if err != nil {
		panic(err)
	}

	return ss
}

func ParseToken(token string) error {
	decodedToken, err := jwt.ParseWithClaims(token, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return getSecretKey(), nil
	})

	if _, ok := decodedToken.Claims.(*jwt.StandardClaims); ok && decodedToken.Valid {
		return nil
	} else {
		return err
	}
}
