module gitlab.com/o-cloud/provider-library

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.6.1
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/kr/text v0.2.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	gitlab.com/o-cloud/catalog v0.0.0-20211026092148-685ec4df5feb
	go.mongodb.org/mongo-driver v1.5.2
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
