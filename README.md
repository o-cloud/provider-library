# Provider library

## Concepts

Provider library provide all functionalities to implement a provider that can interract with a o-cloud cluster.

* Autocreate a provider on deploy
* Provide API to manage provider, scopes and access
* Auto control and delete access in background

### Provider

A provider is key service in o-cloud architecture that can provide data, storage or services of any type.  
Provider is automatically registered on cluster (not in catalog) When you deploy it with this library.  
Only description and images can be updated via API. Provider can also be registered in catalog via API on demand.  

### Permissions

A permission define type of rules that can be attached to resources in scopes.  
For example, a data provider will define only ReadOnly permission in general.  
Permissions can only be declared in code. API can only get existing permissions.  

### Resources

A resource define a shard, a service or a space that your provider can serve.  
Resources can only be declared in code. API can only get existing resources.  

### Scopes

A scope associate permissions and resources in which user can ask for access.  
Library API allow to manage creation, modification and deletion of scopes.  

### Access

User can ask for access token on scope for a delimited period.  
With this token, user can ask for access on this provider for pipeline usage.  
When access token is expired, access token and all access created on provider is automatically deleted by the library.  

## API

* [Get all providers](#get-all-providers)
* [Get provider](#get-provider)
* [Get provider cwl](#get-provider-cwl)
* [Update provider](#update-provider)
* [Register provider in catalog](#register-provider-in-catalog)
* [Unregister provider in catalog](#unregister-provider-in-catalog)
* [Get scopes](#get-scopes)
* [Post scope](#post-scope)
* [Update scope](#update-scope)
* [Delete scope](#delete-scope)
* [Get resources](#get-resources)
* [Get permissions](#get-permissions)
* [Generate access token](#generate-access-token)
* [Create access](#create-access)
* [Delete access token](#delete-access-token)
* [Delete access token on scope](#delete-access-token-on-scope)

## Get All providers

Get all providers is implemented in generic back-end par of the project

You can find the implementation here [Generic back-end providers](https://gitlab.com/o-cloud/generic-backend/-/blob/provider/providers/providers_handler.go).

*URL*: `/api/generic-backend/providers`

*METHOD*: `GET`

*Path Params*: None

*Query Params*: None

### Success Response

*Code*: `200 OK`

*Content*: [Providers model](https://gitlab.com/o-cloud/generic-backend/-/blob/provider/providers/providers_type.go)

### Error Response

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl 'http://localhost:9000/api/generic-backend/providers' \
  -H 'Accept: application/json, text/plain, */*' \
   --compressed \
  --insecure
```

## Get provider

*URL*: `/api/providers/{name}/{version}/provider`

*METHOD*: `GET`

*Path Params*:

* name

  * description: provider name we want to update
  * required: true
  * type: string
  * restriction: None

* version
  * description: provider version we want to update
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

### Success Response

*Code*: `200 OK`

*Content*: [Provider model](models/provider/provider.go)

### Error Response

*Code*: `404 Not Found`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl 'http://localhost:9000/api/providers/provider/1.0/provider' \
  -H 'Accept: application/json, text/plain, */*' \
   --compressed \
  --insecure
```

## Get provider cwl

*URL*: `/api/providers/{name}/{version}/provider/cwl`

*METHOD*: `GET`

*Path Params*:

* name

  * description: provider name we want to update
  * required: true
  * type: string
  * restriction: None

* version
  * description: provider version we want to update
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

### Success Response

*Code*: `200 OK`

*Content*: { "cwl" : `cwl` } 

### Error Response

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl 'http://localhost:9000/api/providers/provider/1.0/provider/cwl' \
  -H 'Accept: application/json, text/plain, */*' \
   --compressed \
  --insecure
```

## Update provider

*URL*: `/api/providers/{name}/{version}/provider`

*METHOD*: `PUT`

*Path Params*:

* name

  * description: provider name we want to update
  * required: true
  * type: string
  * restriction: None

* version
  * description: provider version we want to update
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Body*: [Provider model](models/provider/provider.go)

### Success Response

*Code*: `200 OK`

*Content*: [Provider model](models/provider/provider.go)

### Error Response

*Code*: `422 Unprocessable entity`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl 'http://localhost:9000/api/providers/provider/1.0/provider' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'Content-Type: application/json' \
  --data-raw '{
    "provider": {
        "catalogSyncStatus": 0,
        "createdAt": "2021-12-07T14:12:28.021Z",
        "description": "Description for a new provider removal 2",
        "images": ["https://www.irt-saintexupery.com/wp-content/uploads/2021/02/logo_IRT-Saint-Exupery.png"],
        "name": "provider",
        "registeredAt": "2021-12-08T09:29:40.338Z",
        "type": "data",
        "updatedAt": "2021-12-08T09:40:08.609Z",
        "version": "1.0"
    }
}' \
 --compressed \
  --insecure
```

## Register provider in catalog

*URL*: `/api/providers/{name}/{version}/provider/register`

*METHOD*: `POST`

*Path Params*:

* name

  * description: name of the provider we want to add to the catalog
  * required: true
  * type: string
  * restriction: None

* version
  * description: name of the provider we want to add to the catalog
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Body*: [Provider model](models/provider/provider.go)

### Success Response

*Code*: `200 OK`

*Content*: [Provider model](models/provider/provider.go)

### Error Response

*Code*: `422 Unprocessable entity`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl 'http://localhost:9000/api/providers/provider/1.0/provider/register' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'Content-Type: application/json' \
  --data-raw '{
    "provider":
{
            "name": "provider",
            "version": "1.1",
            "type": "data",
            "catalogSyncStatus": 1,
            "createdAt": "2021-11-04T15:09:40.507Z",
            "updatedAt": "2021-11-04T15:09:40.507Z"
        }
        }' \
 --compressed \
  --insecure
```

## Unregister provider in catalog

*URL*: `/api/providers/{name}/{version}/provider/unregister`

*METHOD*: `POST`

*Path Params*:

* name

  * description: name of the provider we want to remove from the catalog
  * required: true
  * type: string
  * restriction: None

* version
  * description: version of the provider we want to remove from the catalog
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Body*: [Provider model](models/provider/provider.go)

### Success Response

*Code*: `200 OK`

*Content*: [Provider model](models/provider/provider.go)

### Error Response

*Code*: `422 Unprocessable entity`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl 'http://localhost:9000/api/providers/provider/1.0/provider/register' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'Content-Type: application/json' \
  --data-raw '{
    "provider":
{
            "name": "provider",
            "version": "1.1",
            "type": "data",
            "catalogSyncStatus": 1,
            "createdAt": "2021-11-04T15:09:40.507Z",
            "updatedAt": "2021-11-04T15:09:40.507Z"
        }
        }' \
 --compressed \
  --insecure
```

## Get scopes

*URL*: `/api/providers/{name}/{version}/provider/scopes`

*METHOD*: `GET`

*Path Params*:

* name

  * description: name of the provider on which we want to have the scopes
  * required: true
  * type: string
  * restriction: None

* version
  * description: version of the provider on which we want to have the scopes
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Body*: [Scope model](models/scopes/scopes.go)

### Success Response

*Code*: `200 OK`

*Content*: [Scope model](models/scopes/scopes.go)

### Error Response

*Code*: `422 Unprocessable entity`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl 'http://localhost:9000/api/providers/provider/1.0/provider/scopes' \
  -H 'Accept: application/json, text/plain, */*' \
  --compressed \
  --insecure
```

## Post scope

*URL*: `/api/providers/{name}/{version}/provider/scopes`

*METHOD*: `POST`

*Path Params*:

* name

  * description: name of the provider on which we want to add a new scope
  * required: true
  * type: string
  * restriction: None

* version
  * description: version of the provider on which we want to add a new scop
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Body*: [Scope model](models/scopes/scopes.go)

### Success Response

*Code*: `200 OK`

*Content*: [Scope model](models/scopes/scopes.go)

### Error Response

*Code*: `422 Unprocessable entity`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl 'http://localhost:9000/api/providers/provider/1.0/provider/register' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'Content-Type: application/json' \
  --data-raw '{
    "scope":
    {"name":"Scope2","description":"Description scope ","resources":["providers","access_tokens"],"permissions":["ReadOnly"]}
    }' \
 --compressed \
  --insecure
```

## Update scope

*URL*: `/api/providers/{name}/{version}/provider/scopes/{id}`

*METHOD*: `PUT`

*Path Params*:

* name

  * description: name of the provider on which we want to add a new scope
  * required: true
  * type: string
  * restriction: None

* version
  * description: version of the provider on which we want to add a new scope
  * required: true
  * type: string
  * restriction: None

* id
  * description: if of the scope we want to update
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Body*: [Scope model](models/scopes/scopes.go)

### Success Response

*Code*: `200 OK`

*Content*: [Scope model](models/scopes/scopes.go)

### Error Response

*Code*: `422 Unprocessable entity`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl '/api/providers/provider/1.0/provider/scopes/61af31b6b983dc59aa743607' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'Content-Type: application/json' \
  --data-raw '{
    "scope":
    {"name":"test13","description":"Description scope number 11 ","resources":["providers","access_tokens"],"permissions":["ReadOnly"]}
    }' \
 --compressed \
  --insecure
```

## Delete scope

*URL*: `/api/providers/{name}/{version}/provider/scopes/{id}`

*METHOD*: `DELETE`

*Path Params*:

* name

  * description: name of the provider on which we want to remove a scope
  * required: true
  * type: string
  * restriction: None

* version
  * description: version of the provider on which we want to remove a scope
  * required: true
  * type: string
  * restriction: None

* id
  * description: id of the scope we want to remove
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Body*: [Scope model](models/scopes/scopes.go)

### Success Response

*Code*: `204 No Content`

*Content*: [Scope model](models/scopes/scopes.go)

### Error Response

*Code*: `404 Not Found`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl '/api/providers/provider/1.0/provider/scopes/61af31b6b983dc59aa743607' \
  -X 'DELETE' \
  -H 'Accept: application/json, text/plain, */*' \
  --compressed \
  --insecure
```

## Get resources

*URL*: `/api/providers/{name}/{version}/resources`

*METHOD*: `GET`

*Path Params*:

* name

  * description: name of the provider on which we want to have the resources
  * required: true
  * type: string
  * restriction: None

* version
  * description: version of the provider on which we want to have the resources
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Body*: [Resources model](models/resources/resources.go)

### Success Response

*Code*: `200 OK`

*Content*: [Resources model](models/resources/resources.go)

### Error Response

*Code*: `404 Not Found`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl 'http://localhost:9000/api/providers/provider/1.0/resources' \
  -H 'Accept: application/json, text/plain, */*' \
  --compressed \
  --insecure
```

## Get permissions

*URL*: `/api/providers/{name}/{version}/permissions`

*METHOD*: `GET`

*Path Params*:

* name

  * description: name of the provider on which we want to have the permissions
  * required: true
  * type: string
  * restriction: None

* version
  * description: version of the provider on which we want to have the permissions
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Body*: [Permissions model](models/permissions/permissions.go)

### Success Response

*Code*: `200 OK`

*Content*: [Permissions model](models/permissions/permissions.go)

### Error Response

*Code*: `404 Not Found`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl 'http://localhost:9000/api/providers/provider/1.0/permissions' \
  -H 'Accept: application/json, text/plain, */*' \
  --compressed \
  --insecure
```

## Generate access token

*URL*: `/api/providers/{name}/{version}/provider/access/generate-token`

*METHOD*: `POST`

*Path Params*:

* name

  * description: name of the provider on which we want to have the permissions
  * required: true
  * type: string
  * restriction: None

* version
  * description: version of the provider on which we want to have the permissions
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Body*: [Access tokens model](models/access_tokens/access_tokens.go)

### Success Response

*Code*: `200 OK`

*Content*: JWT token

### Error Response

*Code*: `422 Unprocessable entity`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl '/api/providers/provider/1.0/provider/access/generate-token' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'Content-Type: application/json' \
  --data-raw '{
    "access": {
        "scope": "61c205933612a0244753f5e1",
        "identities":["identity_name"],
        "endDate": "2022-12-22T00:00:00Z"
    }
  }' \
 --compressed \
  --insecure
```

## Create access

*URL*: `/api/providers/{name}/{version}/provider/access`

*METHOD*: `POST`

*Path Params*:

* name

  * description: name of the provider on which we want to have the permissions
  * required: true
  * type: string
  * restriction: None

* version
  * description: version of the provider on which we want to have the permissions
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Header*: Authorization JWT token bearer

*Request Body*: None

### Success Response

*Code*: `200 OK`

*Content*: Access object

### Error Response

*Code*: `403 Forbidden`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl '/api/providers/provider/1.0/provider/access' \
  -X 'POST' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDAxMzEyMDAsImlzcyI6InByb3ZpZGVyLW1vbmdvZGItMS4wIn0.u3fi-tmMxZXkfmkYpUEvNihz7L6JSgRS5FQRb-XEgxA' \
 --compressed \
  --insecure
```

## Delete access token

*URL*: `/api/providers/{name}/{version}/provider/access`

*METHOD*: `DELETE`

*Path Params*:

* name

  * description: name of the provider on which we want to have the permissions
  * required: true
  * type: string
  * restriction: None

* version
  * description: version of the provider on which we want to have the permissions
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Header*: Authorization JWT token bearer

*Request Body*: None

### Success Response

*Code*: `204 No Content`

*Content*: None

### Error Response

*Code*: `403 Forbidden`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl '/api/providers/provider/1.0/provider/access' \
  -X 'DELETE' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDAxMzEyMDAsImlzcyI6InByb3ZpZGVyLW1vbmdvZGItMS4wIn0.u3fi-tmMxZXkfmkYpUEvNihz7L6JSgRS5FQRb-XEgxA' \
 --compressed \
  --insecure
```

## Delete access token on scope

*URL*: `/api/providers/{name}/{version}/provider/access/scopes/{scope_id}`

*METHOD*: `DELETE`

*Path Params*:

* scope_id

  * description: id of the scope on which we want to delete all access
  * required: true
  * type: string
  * restriction: None

* name

  * description: name of the provider on which we want to have the permissions
  * required: true
  * type: string
  * restriction: None

* version
  * description: version of the provider on which we want to have the permissions
  * required: true
  * type: string
  * restriction: None

*Query Params*: None

*Request Header*: Authorization JWT token bearer

*Request Body*: None

### Success Response

*Code*: `204 No Content`

*Content*: None

### Error Response

*Code*: `403 Forbidden`

Or

*Code*: `500 Internal Server Error`

### Sample Call

```bash
  curl '/api/providers/provider/1.0/provider/access/scopes/61ae4e6580967f2f95c44f30' \
  -X 'DELETE' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDAxMzEyMDAsImlzcyI6InByb3ZpZGVyLW1vbmdvZGItMS4wIn0.u3fi-tmMxZXkfmkYpUEvNihz7L6JSgRS5FQRb-XEgxA' \
 --compressed \
  --insecure
```
