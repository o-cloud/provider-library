package common

import (
	"github.com/gin-gonic/gin/binding"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
)

func RegisterValidator(validatorFn validator.Func, name string, errText string) {
	v := binding.Validator.Engine().(*validator.Validate)
	v.RegisterValidation(name, validatorFn)

	registerFn := func(ut ut.Translator) error {
		return ut.Add(name, errText, false)

	}
	transFn := func(ut ut.Translator, fe validator.FieldError) string {
		t, err := ut.T(fe.Tag(), fe.Field())
		if err != nil {
			return fe.(error).Error()
		}
		return t
	}
	v.RegisterTranslation(name, *trans, registerFn, transFn)
}
