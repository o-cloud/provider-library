package common

import (
	"context"
	"time"

	"gitlab.com/o-cloud/provider-library/config"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const ProvidersCollection string = "providers"
const AccessTokensCollection string = "access_tokens"
const AccessObjectCollection string = "access_object"

var MongoClient *mongo.Database

func LoadMongoClient() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(config.Config.MongoDatabase.ConString))
	if err != nil {
		panic("Error init MongoDB client: " + err.Error())
	}

	MongoClient = client.Database(config.Config.MongoDatabase.Name)

	createProvidersUniqueIndex()
	createAccessTokensUniqueIndex()
	createAccessTokenScopeIndex()
	createAccessTokenDateIndex()
	createAccessObjectIndex()
}

func createProvidersUniqueIndex() {
	_, err := MongoClient.Collection(ProvidersCollection).Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys:    bson.D{{Key: "name", Value: 1}, {Key: "version", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
	)

	if err != nil && !mongo.IsDuplicateKeyError(err) {
		panic("Error init index: " + err.Error())
	}
}

func createAccessTokensUniqueIndex() {
	_, err := MongoClient.Collection(AccessTokensCollection).Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys:    bson.D{{Key: "signature", Value: 1}, {Key: "provider_name", Value: 1}, {Key: "provider_version", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
	)

	if err != nil && !mongo.IsDuplicateKeyError(err) {
		panic("Error init index: " + err.Error())
	}
}

func createAccessObjectIndex() {
	_, err := MongoClient.Collection(AccessObjectCollection).Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys: bson.D{{Key: "access_token_id", Value: 1}},
		},
	)

	if err != nil && !mongo.IsDuplicateKeyError(err) {
		panic("Error init index: " + err.Error())
	}
}

func createAccessTokenScopeIndex() {
	_, err := MongoClient.Collection(AccessObjectCollection).Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys: bson.D{{Key: "scope_id", Value: 1}, {Key: "provider_name", Value: 1}, {Key: "provider_version", Value: 1}},
		},
	)

	if err != nil && !mongo.IsDuplicateKeyError(err) {
		panic("Error init index: " + err.Error())
	}
}

func createAccessTokenDateIndex() {
	_, err := MongoClient.Collection(AccessObjectCollection).Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys: bson.D{{Key: "end_at", Value: 1}, {Key: "provider_name", Value: 1}, {Key: "provider_version", Value: 1}},
		},
	)

	if err != nil && !mongo.IsDuplicateKeyError(err) {
		panic("Error init index: " + err.Error())
	}
}

func HexToPrimitiveIdRef(idHex string) primitive.ObjectID {
	id, err := primitive.ObjectIDFromHex(idHex)
	if err != nil {
		panic(err)
	}

	return id
}

// Using this function to get a connection, you can create your connection pool here.
func GetMongoClient() *mongo.Database {
	return MongoClient
}
