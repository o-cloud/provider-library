package common

import (
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

var (
	trans *ut.Translator
)

func init() {
	english := en.New()
	uni := ut.New(english, english)
	translator, _ := uni.GetTranslator("en")
	trans = &translator

	v := binding.Validator.Engine().(*validator.Validate)
	_ = en_translations.RegisterDefaultTranslations(v, *trans)
}

func GetTranslator() *ut.Translator {
	return trans
}
