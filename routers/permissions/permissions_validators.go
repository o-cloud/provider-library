package routers

import (
	"github.com/go-playground/validator/v10"
	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/models/permissions"
)

func registerValidators() {
	validatorFn := func(fl validator.FieldLevel) bool {
		return permissions.GetAccessPermissionsRepository().CheckPermissionExist(fl.Field().String())
	}
	common.RegisterValidator(validatorFn, "validPermission", "{0} must be a valid access permission")
}
