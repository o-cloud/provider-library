package routers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/o-cloud/provider-library/models/permissions"
)

type AccessPermissionsHandler struct {
	A permissions.AccessPermissionsRepository
}

func NewAccessPermissionsHandler() *AccessPermissionsHandler {
	registerValidators()
	return &AccessPermissionsHandler{
		A: permissions.GetAccessPermissionsRepository(),
	}
}

// SetupRoutes Define the (sub)routes handled by JSONDecoratorHandler
func (A *AccessPermissionsHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("", A.handleList)
}

func (A *AccessPermissionsHandler) handleList(c *gin.Context) {
	permissions := A.A.GetAccessPermissions()
	serializer := AccessPermissionsSerializer{c, permissions}
	c.JSON(http.StatusOK, serializer.Response())
}
