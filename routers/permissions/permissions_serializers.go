package routers

import (
	"gitlab.com/o-cloud/provider-library/api"
	"gitlab.com/o-cloud/provider-library/models/permissions"

	"github.com/gin-gonic/gin"
)

type AccessPermissionsSerializer struct {
	C                 *gin.Context
	AccessPermissions permissions.AccessPermissions
}

func (s *AccessPermissionsSerializer) Response() api.AccessPermissionsResponse {
	response := []string{}
	for _, permission := range s.AccessPermissions {
		response = append(response, string(permission))
	}
	return api.AccessPermissionsResponse{AccessPermissions: response, AccessPermissionsCount: len(response)}
}
