package routers

import (
	"gitlab.com/o-cloud/provider-library/api"
	"gitlab.com/o-cloud/provider-library/models/scopes"

	"github.com/gin-gonic/gin"
)

type ScopesSerializer struct {
	C      *gin.Context
	Scopes scopes.Scopes
}

type ScopeSerializer struct {
	C     *gin.Context
	Scope scopes.Scope
}

func (s *ScopesSerializer) Response() api.ScopesResponse {
	response := []api.ScopeForResponse{}
	for _, scope := range s.Scopes {
		serializer := ScopeSerializer{s.C, scope}
		response = append(response, serializer.Response())
	}
	return api.ScopesResponse{Scopes: response, ScopesCount: len(response)}
}

func (s *ScopeSerializer) Response() api.ScopeForResponse {
	resources := []api.ResourceForResponse{}
	for _, resource := range s.Scope.Resources {
		resources = append(resources, api.ResourceForResponse{
			Name:     resource.Name,
			Category: resource.Category,
		})
	}
	permissions := []string{}
	for _, permission := range s.Scope.Permissions {
		permissions = append(permissions, string(permission))
	}
	response := api.ScopeForResponse{
		Id:          s.Scope.Id.Hex(),
		Name:        s.Scope.Name,
		Description: s.Scope.Description,
		Resources:   resources,
		Permissions: permissions,
		CreatedAt:   &s.Scope.CreatedAt,
		UpdatedAt:   &s.Scope.UpdatedAt,
	}
	return response
}
