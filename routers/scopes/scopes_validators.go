package routers

import (
	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/models/permissions"
	"gitlab.com/o-cloud/provider-library/models/resources"
	"gitlab.com/o-cloud/provider-library/models/scopes"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

type ScopeRequestValidator struct {
	Scope struct {
		Name        string   `json:"name" binding:"required,min=5,max=128"`
		Description string   `json:"description" binding:"max=512"`
		Resources   []string `json:"resources" binding:"required,min=1,max=50,dive,validResource,max=128"`
		Permissions []string `json:"permissions" binding:"required,min=1,max=50,dive,validPermission,max=128"`
	} `json:"scope"`
	ScopeModel scopes.Scope `json:"-"`
}

func NewScopeValidator() ScopeRequestValidator {
	return ScopeRequestValidator{}
}

func (s *ScopeRequestValidator) Bind(c *gin.Context) error {

	err := c.ShouldBindWith(s, binding.JSON)
	if err != nil {
		return err
	}

	s.ScopeModel.Name = s.Scope.Name
	s.ScopeModel.Description = s.Scope.Description

	s.ScopeModel.Resources = resources.Resources{}
	for _, resource := range s.Scope.Resources {
		providerResource, _ := resources.GetResourcesRepository().GetResource(resource)
		s.ScopeModel.Resources = append(s.ScopeModel.Resources, *providerResource)
	}

	s.ScopeModel.Permissions = permissions.AccessPermissions{}
	for _, permission := range s.Scope.Permissions {
		accessPermission := permissions.AccessPermission(permission)
		s.ScopeModel.Permissions = append(s.ScopeModel.Permissions, accessPermission)
	}

	return nil
}

func registerValidators() {
	validatorFn := func(fl validator.FieldLevel) bool {
		scope, _ := fl.Field().Interface().(string)
		_, err := scopes.GetScopesRepository().GetScopeById(scope)
		return err == nil
	}
	common.RegisterValidator(validatorFn, "validScope", "{0} must be a valid scope")
}
