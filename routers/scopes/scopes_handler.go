package routers

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/models/scopes"
)

type ScopesHandler struct {
	S scopes.ScopesRepository
}

func NewScopesHandler() *ScopesHandler {
	registerValidators()
	return &ScopesHandler{
		S: scopes.GetScopesRepository(),
	}
}

// SetupRoutes Define the (sub)routes handled by JSONDecoratorHandler
func (S *ScopesHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("", S.handleList)
	router.POST("", S.handleCreate)
	router.PUT("/:id", S.handleUpdate)
	router.DELETE("/:id", S.handleDelete)
}

func (S *ScopesHandler) handleList(c *gin.Context) {
	scopes := S.S.ListScopes()
	serializer := ScopesSerializer{c, *scopes}
	c.JSON(http.StatusOK, serializer.Response())
}

func (S *ScopesHandler) handleCreate(c *gin.Context) {
	scopeModelValidator := NewScopeValidator()
	err := scopeModelValidator.Bind(c)
	if err != nil {
		HandleScopeBindingError(c, err)
		return
	}

	if err = S.S.CreateScope(&scopeModelValidator.ScopeModel); err != nil {
		body := common.NewError(common.AlreadyExistsErrorCode, err)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	c.JSON(http.StatusOK, gin.H{})
}

func (S *ScopesHandler) handleUpdate(c *gin.Context) {
	id := c.Param("id")

	scopeModelValidator := NewScopeValidator()
	err := scopeModelValidator.Bind(c)
	if err != nil {
		HandleScopeBindingError(c, err)
		return
	}

	if _, err := S.S.GetScopeById(id); err != nil {
		body := common.NewError(common.NotFoundsErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	if err = S.S.UpdateScope(id, &scopeModelValidator.ScopeModel); err != nil {
		body := common.NewError(common.AlreadyExistsErrorCode, err)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	c.JSON(http.StatusOK, gin.H{})
}

func (S *ScopesHandler) handleDelete(c *gin.Context) {
	id := c.Param("id")

	if err := S.S.DeleteScope(id); err != nil {
		body := common.NewError(common.NotFoundsErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	c.Status(http.StatusNoContent)
}

func HandleScopeBindingError(c *gin.Context, err error) {
	errs, ok := err.(validator.ValidationErrors)
	if !ok {
		body := common.NewError(common.BadRequestErrorCode, errors.New("bad request : unable to parse json body"))
		c.JSON(http.StatusBadRequest, body)
		return
	}
	body := common.NewValidatorError(common.BadBodyErrorCode, errs)
	c.JSON(http.StatusUnprocessableEntity, body)
}
