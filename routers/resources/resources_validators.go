package routers

import (
	"github.com/go-playground/validator/v10"
	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/models/resources"
)

func registerValidators() {
	validatorFn := func(fl validator.FieldLevel) bool {
		return resources.GetResourcesRepository().IsResourceExist(fl.Field().String())
	}
	common.RegisterValidator(validatorFn, "validResource", "{0} must be a valid resource")
}
