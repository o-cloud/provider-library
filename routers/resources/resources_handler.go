package routers

import (
	"net/http"

	"gitlab.com/o-cloud/provider-library/models/resources"

	"github.com/gin-gonic/gin"
)

type ResourcesHandler struct {
	R resources.ResourcesRepository
}

func NewResourcesHandler() *ResourcesHandler {
	registerValidators()
	return &ResourcesHandler{
		R: resources.GetResourcesRepository(),
	}
}

// SetupRoutes Define the (sub)routes handled by JSONDecoratorHandler
func (R *ResourcesHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("", R.handleListResources)
}

func (R *ResourcesHandler) handleListResources(c *gin.Context) {
	resources := R.R.ListResources()
	serializer := ResourcesSerializer{c, *resources}
	c.JSON(http.StatusOK, serializer.Response())
}
