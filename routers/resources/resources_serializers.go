package routers

import (
	"gitlab.com/o-cloud/provider-library/api"
	"gitlab.com/o-cloud/provider-library/models/resources"

	"github.com/gin-gonic/gin"
)

type ResourcesSerializer struct {
	C         *gin.Context
	Resources resources.Resources
}

type ResourceSerializer struct {
	C        *gin.Context
	Resource resources.Resource
}

func (r *ResourcesSerializer) Response() api.ResourcesResponse {
	response := []api.ResourceForResponse{}
	for _, resource := range r.Resources {
		serializer := ResourceSerializer{r.C, resource}
		response = append(response, serializer.Response())
	}
	return api.ResourcesResponse{Resources: response, ResourcesCount: len(response)}
}

func (r *ResourceSerializer) Response() api.ResourceForResponse {
	response := api.ResourceForResponse{
		Name:     r.Resource.Name,
		Category: r.Resource.Category,
	}
	return response
}
