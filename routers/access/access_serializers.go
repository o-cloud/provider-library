package routers

import (
	"gitlab.com/o-cloud/provider-library/api"
	"gitlab.com/o-cloud/provider-library/models/access_object"

	"github.com/gin-gonic/gin"
)

type AccessSerializer struct {
	C      *gin.Context
	Access access_object.AccessObject
}

func (a *AccessSerializer) Response() api.AccessResponse {
	response := api.AccessForResponse{
		ConnectionInformation: a.Access.ConnectionInformation,
		EndDate:               &a.Access.EndAt,
	}
	return api.AccessResponse{Access: response}
}
