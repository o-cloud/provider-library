package routers

import (
	"fmt"
	"time"

	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/config"
	"gitlab.com/o-cloud/provider-library/models/access_tokens"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

const layoutdate = time.RFC3339

type AccessTokenRequestValidator struct {
	AccessTokenRequest struct {
		Scope      string                 `json:"scope" binding:"required,validScope,max=512"`
		Identities []string               `json:"identities" binding:"required,unique,gte=1,lte=1000,dive,max=1024"`
		EndDate    string                 `json:"endDate" binding:"omitempty,validEndDateFormat,validEndDateIsFuture,validEndDateIsNotTooLate"`
		Params     map[string]interface{} `json:"params"`
	} `json:"access"`
	AccessTokenModel access_tokens.AccessToken `json:"-"`
}

func NewAccessTokenRequestValidator() AccessTokenRequestValidator {
	return AccessTokenRequestValidator{}
}

func (a *AccessTokenRequestValidator) Bind(c *gin.Context) error {

	err := c.ShouldBindWith(a, binding.JSON)
	if err != nil {
		return err
	}

	endDate, err := time.Parse(layoutdate, a.AccessTokenRequest.EndDate)

	if err != nil {
		panic(err)
	}

	a.AccessTokenModel = access_tokens.AccessToken{
		Scope:      common.HexToPrimitiveIdRef(a.AccessTokenRequest.Scope),
		Identities: a.AccessTokenRequest.Identities,
		EndAt:      endDate,
		Params:     a.AccessTokenRequest.Params,
	}

	return nil
}

func registerValidators() {
	registerEndDateFormatValidator()
	registerEndDateIsFutureValidator()
	registerEndDateIsNotTooLateValidator()
}

func registerEndDateFormatValidator() {
	validatorFn := func(fl validator.FieldLevel) bool {
		_, err := time.Parse(layoutdate, fl.Field().String())
		return err == nil
	}
	common.RegisterValidator(validatorFn, "validEndDateFormat", "{0} must be a RFC3339 formated date")
}

func registerEndDateIsFutureValidator() {
	validatorFn := func(fl validator.FieldLevel) bool {
		endDate, err := time.Parse(layoutdate, fl.Field().String())

		if err != nil {
			panic(err)
		}

		return !time.Now().After(endDate)
	}
	common.RegisterValidator(validatorFn, "validEndDateIsFuture", "{0} must be a future date")
}

func registerEndDateIsNotTooLateValidator() {
	validityDuration := time.Duration(config.Config.TokenValidityHoursDuration) * time.Hour
	expirationDate := time.Now().Add(validityDuration)
	validatorFn := func(fl validator.FieldLevel) bool {
		endDate, err := time.Parse(layoutdate, fl.Field().String())

		if err != nil {
			panic(err)
		}

		return !endDate.After(expirationDate)
	}
	errText := fmt.Sprintf("{0} must be less than %s (%s)", expirationDate.Format(layoutdate), validityDuration.String())
	common.RegisterValidator(validatorFn, "validEndDateIsNotTooLate", errText)
}
