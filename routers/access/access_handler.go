package routers

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/models/access_object"
	"gitlab.com/o-cloud/provider-library/models/access_tokens"
	"gitlab.com/o-cloud/provider-library/services/jwt"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type AccessHandler struct {
	A access_tokens.AccessTokensRepository
	O access_object.AccessObjectRepository
}

func NewProviderHandler() *AccessHandler {
	registerValidators()
	return &AccessHandler{
		A: access_tokens.GetAccessTokensRepository(),
		O: access_object.GetAccessObjectRepository(),
	}
}

// SetupRoutes Define the (sub)routes handled by JSONDecoratorHandler
func (A *AccessHandler) SetupRoutes(router *gin.RouterGroup) {
	router.POST("/generate-token", A.handleGenerateAccessToken)
	router.POST("", A.handleCreateAccess)
	router.DELETE("", A.handleDeleteAccess)
	router.DELETE("/scopes/:id", A.handleDeleteAccessFromScope)

}

func (A *AccessHandler) handleGenerateAccessToken(c *gin.Context) {
	accessTokenModelValidator := NewAccessTokenRequestValidator()
	err := accessTokenModelValidator.Bind(c)
	if err != nil {
		HandleAccessTokenBindingError(c, err)
		return
	}

	ss, err := A.A.CreateAccessToken(&accessTokenModelValidator.AccessTokenModel)
	if err != nil {
		body := common.NewError(common.AlreadyExistsErrorCode, err)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	c.JSON(http.StatusCreated, gin.H{"token": ss})
}

func (A *AccessHandler) handleCreateAccess(c *gin.Context) {
	token, err := GetAuthToken(c)
	if err != nil {
		body := common.NewError(common.ForbiddenErrorCode, err)
		c.JSON(http.StatusForbidden, body)
		return
	}

	accessToken, err := A.A.GetAccessToken(token)
	if err != nil {
		body := common.NewError(common.ForbiddenErrorCode, err)
		c.JSON(http.StatusForbidden, body)
		return
	}

	accessObject, err := A.O.CreateAccessObject(accessToken)
	if err != nil {
		body := common.NewError(common.ForbiddenErrorCode, err)
		c.JSON(http.StatusForbidden, body)
		return
	}

	serializer := AccessSerializer{c, *accessObject}
	c.JSON(http.StatusOK, serializer.Response())
}

func (A *AccessHandler) handleDeleteAccess(c *gin.Context) {
	token, err := GetAuthToken(c)
	if err != nil {
		body := common.NewError(common.ForbiddenErrorCode, err)
		c.JSON(http.StatusForbidden, body)
		return
	}

	accessToken, err := A.A.GetAccessToken(token)
	if err != nil {
		body := common.NewError(common.ForbiddenErrorCode, err)
		c.JSON(http.StatusForbidden, body)
		return
	}

	A.O.DeleteAccessObject(accessToken)

	c.Status(http.StatusNoContent)
}

func (A *AccessHandler) handleDeleteAccessFromScope(c *gin.Context) {
	scopeId := c.Param("id")
	accessTokens := A.A.GetAccessTokenFromScope(scopeId)

	for _, accessToken := range *accessTokens {
		A.O.DeleteAccessObject(&accessToken)
	}

	c.Status(http.StatusNoContent)
}

func HandleAccessTokenBindingError(c *gin.Context, err error) {
	errs, ok := err.(validator.ValidationErrors)
	if !ok {
		body := common.NewError(common.BadRequestErrorCode, errors.New("bad request : unable to parse json body"))
		c.JSON(http.StatusBadRequest, body)
		return
	}
	body := common.NewValidatorError(common.BadBodyErrorCode, errs)
	c.JSON(http.StatusUnprocessableEntity, body)
}

func GetAuthToken(c *gin.Context) (string, error) {
	auth := c.Request.Header.Get("Authorization")
	if auth == "" {
		return "", fmt.Errorf("no Authorization header provided")
	}
	token := strings.TrimPrefix(auth, "Bearer ")
	if token == auth {
		return "", fmt.Errorf("could not find bearer token in Authorization header")
	}

	// Token verification
	err := jwt.ParseToken(token)
	if err != nil {
		return "", fmt.Errorf("unable to validate token: %s", err.Error())
	}

	return token, nil
}
