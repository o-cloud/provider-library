package routers

import (
	"gitlab.com/o-cloud/provider-library/api"
	"gitlab.com/o-cloud/provider-library/models/provider"

	"github.com/gin-gonic/gin"
)

type ProviderSerializer struct {
	C        *gin.Context
	Provider provider.Provider
}

func (p *ProviderSerializer) Response() api.ProviderResponse {
	images := []string{}
	if len(p.Provider.Images) > 0 {
		images = p.Provider.Images
	}
	registeredAt := &p.Provider.RegisteredAt
	if registeredAt.IsZero() {
		registeredAt = nil
	}
	unregisteredAt := &p.Provider.UnregisteredAt
	if unregisteredAt.IsZero() {
		unregisteredAt = nil
	}
	response := api.ProviderForResponse{
		Name:              p.Provider.Name,
		Version:           p.Provider.Version,
		Description:       p.Provider.Description,
		Type:              string(p.Provider.Type),
		Images:            images,
		CatalogRegistered: int(p.Provider.CatalogRegistered),
		CreatedAt:         &p.Provider.CreatedAt,
		UpdatedAt:         &p.Provider.UpdatedAt,
		RegisteredAt:      registeredAt,
		UnregisteredAt:    unregisteredAt,
	}
	return api.ProviderResponse{Provider: response}
}
