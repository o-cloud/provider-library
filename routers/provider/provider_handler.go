package routers

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/o-cloud/provider-library/common"
	"gitlab.com/o-cloud/provider-library/models/provider"
)

type ProviderHandler struct {
	P provider.ProviderRepository
}

func NewProviderHandler() *ProviderHandler {
	return &ProviderHandler{
		P: provider.GetProviderRepository(),
	}
}

// SetupRoutes Define the (sub)routes handled by JSONDecoratorHandler
func (P *ProviderHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("", P.handleGet)
	router.GET("/cwl", P.handleGetCwl)
	router.PUT("", P.handleUpdate)
	router.POST("/register", P.handleRegister)
	router.POST("/unregister", P.handleUnregister)
}

func (P *ProviderHandler) handleGet(c *gin.Context) {
	provider, err := P.P.GetProvider()

	if err != nil {
		body := common.NewError(common.NotFoundsErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	serializer := ProviderSerializer{c, *provider}
	c.JSON(http.StatusOK, serializer.Response())
}

func (P *ProviderHandler) handleGetCwl(c *gin.Context) {
	provider := P.P.GetProviderCwl()
	c.JSON(http.StatusOK, gin.H{"cwl": provider})
}

func (P *ProviderHandler) handleUpdate(c *gin.Context) {
	providerModelValidator := NewProviderValidator()
	err := providerModelValidator.Bind(c)
	if err != nil {
		HandleProviderBindingError(c, err)
		return
	}

	if _, err = P.P.GetProvider(); err != nil {
		body := common.NewError(common.NotFoundsErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	if err = P.P.UpdateProvider(&providerModelValidator.ProviderModel); err != nil {
		body := common.NewError(common.AlreadyExistsErrorCode, err)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	c.JSON(http.StatusOK, gin.H{})
}

func (P *ProviderHandler) handleRegister(c *gin.Context) {
	provider, err := P.P.GetProvider()

	if err != nil {
		body := common.NewError(common.NotFoundsErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	P.P.RegisterProvider(provider)

	c.JSON(http.StatusOK, gin.H{})
}

func (P *ProviderHandler) handleUnregister(c *gin.Context) {
	provider, err := P.P.GetProvider()

	if err != nil {
		body := common.NewError(common.NotFoundsErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	P.P.UnregisterProvider(provider)

	c.JSON(http.StatusOK, gin.H{})
}

func HandleProviderBindingError(c *gin.Context, err error) {
	errs, ok := err.(validator.ValidationErrors)
	if !ok {
		body := common.NewError(common.BadRequestErrorCode, errors.New("bad request : unable to parse json body"))
		c.JSON(http.StatusBadRequest, body)
		return
	}
	body := common.NewValidatorError(common.BadBodyErrorCode, errs)
	c.JSON(http.StatusUnprocessableEntity, body)
}
