package routers

import (
	"gitlab.com/o-cloud/provider-library/models/provider"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

type ProviderRequestValidator struct {
	Provider struct {
		Description string   `json:"description" binding:"max=512"`
		Images      []string `json:"images" binding:"max=10,dive,url"`
	} `json:"provider"`
	ProviderModel provider.Provider `json:"-"`
}

func NewProviderValidator() ProviderRequestValidator {
	return ProviderRequestValidator{}
}

func (p *ProviderRequestValidator) Bind(c *gin.Context) error {

	err := c.ShouldBindWith(p, binding.JSON)
	if err != nil {
		return err
	}

	p.ProviderModel.Description = p.Provider.Description
	p.ProviderModel.Images = p.Provider.Images

	return nil
}
