package api

import (
	"time"

	"gitlab.com/o-cloud/provider-library/models/access_object"
)

type AccessPermissionsResponse struct {
	AccessPermissions      []string `json:"permissions" binding:"required"`
	AccessPermissionsCount int      `json:"permissionsCount" binding:"required"`
}

type ResourcesResponse struct {
	Resources      []ResourceForResponse `json:"resources" binding:"required"`
	ResourcesCount int                   `json:"resourcesCount" binding:"required"`
}

type ResourceForResponse struct {
	Name     string `json:"name" binding:"required"`
	Category string `json:"category,omitempty"`
}

type ScopesResponse struct {
	Scopes      []ScopeForResponse `json:"scopes" binding:"required"`
	ScopesCount int                `json:"scopesCount" binding:"required"`
}

type ScopeForResponse struct {
	Id string `json:"id" binding:"required"`

	Name        string                `json:"name" binding:"required"`
	Description string                `json:"description"`
	Resources   []ResourceForResponse `json:"resources" binding:"required"`
	Permissions []string              `json:"permissions" binding:"required"`

	CreatedAt *time.Time `json:"createdAt" binding:"required"`
	UpdatedAt *time.Time `json:"updatedAt" binding:"required"`
}

type ProviderResponse struct {
	Provider ProviderForResponse `json:"provider" binding:"required"`
}

type ProviderForResponse struct {
	Name        string   `json:"name" binding:"required"`
	Version     string   `json:"version" binding:"required"`
	Description string   `json:"description"`
	Type        string   `json:"type" binding:"required"`
	Images      []string `json:"images"`

	CatalogRegistered int        `json:"catalogSyncStatus" binding:"required"`
	CreatedAt         *time.Time `json:"createdAt" binding:"required"`
	UpdatedAt         *time.Time `json:"updatedAt" binding:"required"`
	RegisteredAt      *time.Time `json:"registeredAt,omitempty"`
	UnregisteredAt    *time.Time `json:"unregisteredAt,omitempty"`
}

type AccessResponse struct {
	Access AccessForResponse `json:"access" binding:"required"`
}

type AccessForResponse struct {
	access_object.ConnectionInformation `json:"connection" binding:"required"`
	EndDate                             *time.Time `json:"endAt,omitempty"`
}
